#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QLabel>
#include <QDateTimeEdit>
#include <QDateEdit>
#include <QDate>
#include <QGraphicsView>
#include <QtMath>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    //These variables allow the user to select the Planets and units evaluated
    int Planet1ID = 3;
    int Planet2ID = 4;
    int units = 1;

    //This is the default date used to measure the initial location of the planets
    QDate d1 = QDate(2016,9,22);

    //These are the locations of the planets at d1 and the angular velocity of the planet
    int MercuryR = 57909050; //kilometers
    double MercuryTH0 = M_PI/6; //radians
    double MercuryW = 2*M_PI/87.969; //radians per day

    int VenusR = 108208000;
    double VenusTH0 = 250*M_PI/180;
    double VenusW = 2*M_PI/224.701;

    int EarthR = 149598023;
    double EarthTH0 = 0;
    double EarthW = 2*M_PI/365.256;

    int MarsR = 227939200;
    double MarsTH0 = 7*M_PI/4;
    double MarsW = 2*M_PI/686.971;

    int JupiterR = 778299000;
    double JupiterTH0 = 190*M_PI/180;
    double JupiterW = 2*M_PI/4332.59;

    int SaturnR = 1429390000;
    double SaturnTH0 = 260*M_PI/180;
    double SaturnW = 2*M_PI/10759.22;

    int UranusR = 2875040000;
    double UranusTH0 = M_PI/12;
    double UranusW = 2*M_PI/30688.5;

    double NeptuneR = 4504450000;
    double NeptuneTH0 = 11*M_PI/12;
    double NeptuneW = 2*M_PI/60182;

private slots:
    //Push buttons
    void on_About_released();
    void on_Test_released();
    void on_Calculate_released();

    //Menu items - planets
    void on_actionMercury_3_triggered();
    void on_actionMercury_4_triggered();
    void on_actionVenus_3_triggered();
    void on_actionVenus_4_triggered();
    void on_actionEarth_3_triggered();
    void on_actionEarth_4_triggered();
    void on_actionMars_3_triggered();
    void on_actionMars_4_triggered();
    void on_actionJupiter_3_triggered();
    void on_actionJupiter_4_triggered();
    void on_actionSaturn_3_triggered();
    void on_actionSaturn_4_triggered();
    void on_actionUranus_3_triggered();
    void on_actionUranus_4_triggered();
    void on_actionNeptune_3_triggered();
    void on_actionNeptune_4_triggered();

    //Menu items - units
    void on_actionKilometers_2_triggered();
    void on_actionMiles_2_triggered();
    void on_actionAU_2_triggered();
    void on_actionLight_Minutes_2_triggered();
    void on_actionMarathons_2_triggered();
    void on_actionSuns_2_triggered();
    void on_actionEarths_2_triggered();

private:
    //Graphics items for the display
    Ui::MainWindow *ui;

    QGraphicsScene* scene;
    QGraphicsEllipseItem* sun;
    QGraphicsEllipseItem* merO;
    QGraphicsEllipseItem* venO;
    QGraphicsEllipseItem* earO;
    QGraphicsEllipseItem* marO;
    QGraphicsEllipseItem* jupO;
    QGraphicsEllipseItem* satO;
    QGraphicsEllipseItem* uraO;
    QGraphicsEllipseItem* nepO;
    QGraphicsEllipseItem* mer;
    QGraphicsEllipseItem* ven;
    QGraphicsEllipseItem* ear;
    QGraphicsEllipseItem* mar;
    QGraphicsEllipseItem* jup;
    QGraphicsEllipseItem* sat;
    QGraphicsEllipseItem* ura;
    QGraphicsEllipseItem* nep;

};

#endif // MAINWINDOW_H
