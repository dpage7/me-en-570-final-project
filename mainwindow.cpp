#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QColor>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_About_released()
{
    QMessageBox about;
    about.about(this,"About","Version: 1.0\n\nDeveloped by David Page\nfor ME EN 570: Advanced CAE\n\nPlease, let me pass this class.");
    return;
}

void MainWindow::on_Calculate_released()
{
    //This function clears and recalculates both the position and the distance calculation.
    //It then plots the planets and gives data about the planets in a popup.
    scene->clear();

    //Uses the selected date on the calendar widget.
    QDate d2 =ui->calendarWidget->selectedDate();
    int time = d1.daysTo(d2);

    double MerX = MercuryR*cos(MercuryTH0+MercuryW*time);
    double MerY = MercuryR*sin(MercuryTH0+MercuryW*time);
    double VenX = VenusR*cos(VenusTH0+VenusW*time);
    double VenY = VenusR*sin(VenusTH0+VenusW*time);
    double EarX = EarthR*cos(EarthTH0+EarthW*time);
    double EarY = EarthR*sin(EarthTH0+EarthW*time);
    double MarX = MarsR*cos(MarsTH0+MarsW*time);
    double MarY = MarsR*sin(MarsTH0+MarsW*time);
    double JupX = JupiterR*cos(JupiterTH0+JupiterW*time);
    double JupY = JupiterR*sin(JupiterTH0+JupiterW*time);
    double SatX = SaturnR*cos(SaturnTH0+SaturnW*time);
    double SatY = SaturnR*sin(SaturnTH0+SaturnW*time);
    double UraX = UranusR*cos(UranusTH0+UranusW*time);
    double UraY = UranusR*sin(UranusTH0+UranusW*time);
    double NepX = NeptuneR*cos(NeptuneTH0+NeptuneW*time);
    double NepY = NeptuneR*sin(NeptuneTH0+NeptuneW*time);

    double x1;
    double y1;
    double x2;
    double y2;

    QString plan1;
    QString plan2;
    QString info1;
    QString info2;

    //Information about the planets.
    if(Planet1ID == 1)
    {plan1 = "Mercury";
        info1 = "Mercury\n\nDiameter: 4,879 km\nDay: 58.6 Days\nYear: .240 Years\n\n";
    }
    else if(Planet1ID == 2)
    {plan1 = "Venus";
        info1 = "Venus\n\nDiameter: 12,104 km\nDay: -243.0 Days\nYear: .615 Years\n\n";
    }
    else if(Planet1ID == 3)
    {plan1 = "Earth";
        info1 = "Earth\n\nDiameter: 12,745 km\nDay: 1.00 Days\nYear: 1.00 Years\n\n";
    }
    else if(Planet1ID == 4)
    {plan1 = "Mars";
        info1 = "Mars\n\nDiameter: 6,779 km\nDay: 1.026 Days\nYear: 1.881 Years\n\n";}
    else if(Planet1ID == 5)
    {plan1 = "Jupiter";
        info1 = "Jupiter\n\nDiameter: 139,822 km\nDay: 0.414 Days\nYear: 11.86 Years\n\n";
    }
    else if(Planet1ID == 6)
    {plan1 = "Saturn";
        info1 = "Saturn\n\nDiameter: 116,464 km\nDay: 0.440 Days\nYear: 29.46 Years\n\n";
    }
    else if(Planet1ID == 7)
    {plan1 = "Uranus";
        info1 = "Uranus\n\nDiameter: 50,724 km\nDay: 0.718 Days\nYear: 84.0 Years\n\n";
    }
    else if(Planet1ID == 8)
    {plan1 = "Neptune";
        info1 = "Neptune\n\nDiameter: 49,244 km\nDay: 0.671 Days\nYear: 164.8 Years\n\n";
    }

    if(Planet2ID == 1)
    {plan2 = "Mercury";
        info2 = "Mercury\n\nDiameter: 4,879 km\nDay: 58.6 Days\nYear: .240 Years";
    }
    else if(Planet2ID == 2)
    {plan2 = "Venus";
        info2 = "Venus\n\nDiameter: 12,104 km\nDay: -243.0 Days\nYear: .615 Years";
    }
    else if(Planet2ID == 3)
    {plan2 = "Earth";
        info2 = "Earth\n\nDiameter: 12,745 km\nDay: 1.00 Days\nYear: 1.00 Years";
    }
    else if(Planet2ID == 4)
    {plan2 = "Mars";
        info2 = "Mars\n\nDiameter: 6,779 km\nDay: 1.026 Days\nYear: 1.881 Years";
    }
    else if(Planet2ID == 5)
    {plan2 = "Jupiter";
        info2 = "Jupiter\n\nDiameter: 139,822 km\nDay: 0.414 Days\nYear: 11.86 Years";
    }
    else if(Planet2ID == 6)
    {plan2 = "Saturn";
        info2 = "Saturn\n\nDiameter: 116,464 km\nDay: 0.440 Days\nYear: 29.46 Years";
    }
    else if(Planet2ID == 7)
    {plan2 = "Uranus";
        info2 = "Uranus\n\nDiameter: 50,724 km\nDay: 0.718 Days\nYear: 84.0 Years";
    }
    else if(Planet2ID == 8)
    {plan2 = "Neptune";
        info2 = "Neptune\n\nDiameter: 49,244 km\nDay: 0.671 Days\nYear: 164.8 Years";
    }

    //Picks out the values for calculating distance.
    if(Planet1ID == 1)
    {x1 = MerX;y1 = MerY;}
    else if(Planet1ID == 2)
    {x1 = VenX;y1 = VenY;}
    else if(Planet1ID == 3)
    {x1 = EarX;y1 = EarY;}
    else if(Planet1ID == 4)
    {x1 = MarX;y1 = MarY;}
    else if(Planet1ID == 5)
    {x1 = JupX;y1 = JupY;}
    else if(Planet1ID == 6)
    {x1 = SatX;y1 = SatY;}
    else if(Planet1ID == 7)
    {x1 = UraX;y1 = UraY;}
    else if(Planet1ID == 8)
    {x1 = NepX;y1 = NepY;}

    if(Planet2ID == 1)
    {x2 = MerX;y2 = MerY;}
    else if(Planet2ID == 2)
    {x2 = VenX;y2 = VenY;}
    else if(Planet2ID == 3)
    {x2 = EarX;y2 = EarY;}
    else if(Planet2ID == 4)
    {x2 = MarX;y2 = MarY;}
    else if(Planet2ID == 5)
    {x2 = JupX;y2 = JupY;}
    else if(Planet2ID == 6)
    {x2 = SatX;y2 = SatY;}
    else if(Planet2ID == 7)
    {x2 = UraX;y2 = UraY;}
    else if(Planet2ID == 8)
    {x2 = NepX;y2 = NepY;}

    //Calculates the distance.
    double dist = sqrt(pow(x2-x1,2) + pow(y2-y1,2));
    QString unit;

    //Converts units from kilometers to other units
    if(units == 1)
    {dist = dist;}
    else if(units == 2)
    {dist = dist*0.6214;}
    else if(units == 3)
    {dist = dist/(1.496*pow(10,8));}
    else if(units == 4)
    {dist = dist/(17.99*pow(10,6));}
    else if(units == 5)
    {dist = dist/42.195;}
    else if(units == 6)
    {dist = dist/1391400;}
    else if(units == 7)
    {dist = dist/12742;}

    if(units == 1)
    {unit = "Kilometers";}
    else if(units == 2)
    {unit = "Miles";}
    else if(units == 3)
    {unit = "AU";}
    else if(units == 4)
    {unit = "Light-Minutes";}
    else if(units == 5)
    {unit = "Marathons";}
    else if(units == 6)
    {unit = "Suns";}
    else if(units == 7)
    {unit = "Earths";}

    QString distance = QString::number(dist);

    //Plotting parameters
    QBrush red(Qt::red);
    QBrush blue(Qt::blue);
    QBrush venus(QColor(238,232,170,255));
    QBrush jupiter(QColor(245,96,33,255));
    QBrush black(Qt::black);
    QBrush yellow(Qt::yellow);
    QBrush transparent(QColor(0,0,0,0));
    QBrush gray (QColor(139,137,137,255));

    QPen pen(black,1);
    QPen pen2(red,2);

    //Plots the solar system (including the orbits).
    //It has been scaled down to fit in the width of the screen.
    sun = scene->addEllipse(-2,-2,5,5,pen,yellow);

    merO = scene->addEllipse(-MercuryR/8000000,-MercuryR/8000000,MercuryR/4000000,MercuryR/4000000,pen,transparent);
    venO = scene->addEllipse(-VenusR/8000000,-VenusR/8000000,VenusR/4000000,VenusR/4000000,pen,transparent);
    earO = scene->addEllipse(-EarthR/8000000,-EarthR/8000000,EarthR/4000000,EarthR/4000000,pen,transparent);
    marO = scene->addEllipse(-MarsR/8000000,-MarsR/8000000,MarsR/4000000,MarsR/4000000,pen,transparent);
    jupO = scene->addEllipse(-JupiterR/8000000,-JupiterR/8000000,JupiterR/4000000,JupiterR/4000000,pen,transparent);
    satO = scene->addEllipse(-SaturnR/8000000,-SaturnR/8000000,SaturnR/4000000,SaturnR/4000000,pen,transparent);
    uraO = scene->addEllipse(-UranusR/4000000,-UranusR/4000000,UranusR/2000000,UranusR/2000000,pen,transparent);
    nepO = scene->addEllipse(-NeptuneR/8000000,-NeptuneR/8000000,NeptuneR/4000000,NeptuneR/4000000,pen,transparent);

    mer = scene->addEllipse(MerX/8000000-2,-MerY/8000000-2,5,5,pen,gray);
    ven = scene->addEllipse(VenX/8000000-2,-VenY/8000000-2,5,5,pen,venus);
    ear = scene->addEllipse(EarX/8000000-2,-EarY/8000000-2,5,5,pen,blue);
    mar = scene->addEllipse(MarX/8000000-2,-MarY/8000000-2,5,5,pen,red);
    jup = scene->addEllipse(JupX/8000000-2,-JupY/8000000-2,5,5,pen,jupiter);
    sat = scene->addEllipse(SatX/8000000-2,-SatY/8000000-2,5,5,pen,yellow);
    ura = scene->addEllipse(-UraX/4000000-2,UraY/4000000-2,5,5,pen,blue);
    nep = scene->addEllipse(-NepX/8000000-2,NepY/8000000-2,5,5,pen,blue);

    //Popup with the Distance and planet info.
    QMessageBox info;
    info.about(this,"Calculations","The distance from " + plan1 + " to " + plan2 + " is:\n\n" + distance + " " + unit + "\n\n\n" + info1 + info2);
    return;
}

void MainWindow::on_Test_released()
{
    //Allows the user to see what the settings are before preforming the calculation.
    QMessageBox test;
    QString plan1;
    QString plan2;
    QString unit;
    if(Planet1ID == 1)
    {plan1 = "Mercury\n";}
    else if(Planet1ID == 2)
    {plan1 = "Venus\n";}
    else if(Planet1ID == 3)
    {plan1 = "Earth\n";}
    else if(Planet1ID == 4)
    {plan1 = "Mars\n";}
    else if(Planet1ID == 5)
    {plan1 = "Jupiter\n";}
    else if(Planet1ID == 6)
    {plan1 = "Saturn\n";}
    else if(Planet1ID == 7)
    {plan1 = "Uranus\n";}
    else if(Planet1ID == 8)
    {plan1 = "Neptune\n";}

    if(Planet2ID == 1)
    {plan2 = "Mercury\n";}
    else if(Planet2ID == 2)
    {plan2 = "Venus\n";}
    else if(Planet2ID == 3)
    {plan2 = "Earth\n";}
    else if(Planet2ID == 4)
    {plan2 = "Mars\n";}
    else if(Planet2ID == 5)
    {plan2 = "Jupiter\n";}
    else if(Planet2ID == 6)
    {plan2 = "Saturn\n";}
    else if(Planet2ID == 7)
    {plan2 = "Uranus\n";}
    else if(Planet2ID == 8)
    {plan2 = "Neptune\n";}

    if(units == 1)
    {unit = "Kilometers";}
    else if(units == 2)
    {unit = "Miles";}
    else if(units == 3)
    {unit = "AU";}
    else if(units == 4)
    {unit = "Light-Minutes";}
    else if(units == 5)
    {unit = "Marathons";}
    else if(units == 6)
    {unit = "Suns";}
    else if(units == 7)
    {unit = "Earths";}

    QString date1 = d1.toString("MMMM d, yyyy");

    QDate d2 = ui->calendarWidget->selectedDate();

    QString date2 = d2.toString("MMMM d, yyyy");

    QString text = "Planet 1: " + plan1 + "Planet 2: " + plan2 + "Units of Distance: " + unit + "\n" + date2;
    test.about(this,"Settings",text);
    return;
}

//Uses the drop-down menus to change the variables that determine the planets in the calculations
void MainWindow::on_actionMercury_3_triggered()
{
    Planet1ID = 1;
}
void MainWindow::on_actionMercury_4_triggered()
{
    Planet2ID = 1;
}

void MainWindow::on_actionVenus_3_triggered()
{
    Planet1ID = 2;
}
void MainWindow::on_actionVenus_4_triggered()
{
    Planet2ID = 2;
}

void MainWindow::on_actionEarth_3_triggered()
{
    Planet1ID = 3;
}
void MainWindow::on_actionEarth_4_triggered()
{
    Planet2ID = 3;
}

void MainWindow::on_actionMars_3_triggered()
{
    Planet1ID = 4;
}
void MainWindow::on_actionMars_4_triggered()
{
    Planet2ID = 4;
}

void MainWindow::on_actionJupiter_3_triggered()
{
    Planet1ID = 5;
}
void MainWindow::on_actionJupiter_4_triggered()
{
    Planet2ID = 5;
}

void MainWindow::on_actionSaturn_3_triggered()
{
    Planet1ID = 6;
}
void MainWindow::on_actionSaturn_4_triggered()
{
    Planet2ID = 6;
}

void MainWindow::on_actionUranus_3_triggered()
{
    Planet1ID = 7;
}
void MainWindow::on_actionUranus_4_triggered()
{
    Planet2ID = 7;
}

void MainWindow::on_actionNeptune_3_triggered()
{
    Planet1ID = 8;
}
void MainWindow::on_actionNeptune_4_triggered()
{
    Planet2ID = 8;
}

//Uses a drop-down menu to determine the units for the distance calculation
void MainWindow::on_actionKilometers_2_triggered()
{
    units = 1;
}
void MainWindow::on_actionMiles_2_triggered()
{
    units = 2;
}
void MainWindow::on_actionAU_2_triggered()
{
    units = 3;
}
void MainWindow::on_actionLight_Minutes_2_triggered()
{
    units = 4;
}
void MainWindow::on_actionMarathons_2_triggered()
{
    units = 5;
}
void MainWindow::on_actionSuns_2_triggered()
{
    units = 6;
}
void MainWindow::on_actionEarths_2_triggered()
{
    units = 7;
}

